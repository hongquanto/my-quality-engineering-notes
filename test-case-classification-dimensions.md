# Test case classification dimension 

![](https://upload.wikimedia.org/wikipedia/commons/6/69/Coord_system_CA_0.svg) 


+ 🚀 Test Level
    - Unit
    - Integration
    - (Component)
    - System
    - Acceptance

+ 🎛️ Test Type
    - Functional
        * UI CHeck
        * Screenflow 
        * Workflow
    - _(Non-functional)_ 
        * Performance
            - Load test 
            - Stress test 
        * Availability
            - HA / DR testing 
            - Destructive testing 
        * Usability
        * Security 
        * Compactability/Portability
+ ♻️ Retest scope
    - Smoke only 
    - Smoke and Sanity 
    - Smoke and Sanity and Standard
+ 📦 Targeting component
    - API
    - Mobile
        * Android
        * iOS
    - Web
        * Desktop Web
        * Mobile Web 
+ 👣 Execution Technique 
    - Structured Scripted testing 
    - Exploratory testing
    - Ad-hoc testing 
+ ⚒️ Automation Type
    - Tool A
    - Tool B
    - Tool C 
+ 🦠 Lifecycle 
    - Identify
    - Exploring
    - Scripted
    - Automated
    - Outdated
+ 🔥 Priority  
    - Critical
    - High
    - Medium
    - Low 
